// Objeto que guarda todas as informações do servidor
let server = {
  port: 6667,
  version: "1.0.0",
  release: "2017-09-28",
  started: 0, // Timestamp do momento em que o servidor foi iniciado
  password: "",
  pingTimeout: 90, // Segundos
  pongTimeout: 10, // Segundos

  clients: [], // Array de clientes conectados no servidor
  nicks: {}, // Array associativo de nicks usados no servidor
  canais: {}, // Array associativo de canais existentes
  users: {} // Array associativo de usuarios resgistrados
};
module.exports = server;

const net = require('net');
const utils = require('./utils');

const RDG = require('./comandos/registros-de-conexao');
const ODC = require('./comandos/operacoes-de-canal');
const EDM = require('./comandos/envio-de-mensagens');
const COS = require('./comandos/consultas-ao-servidor');
const CBU = require('./comandos/consultas-baseadas-no-usuario');
const VAD = require('./comandos/variadas');

net.createServer(function(socket) {
  socket.name = socket.remoteAddress + ":" + socket.remotePort;
  socket.opened = Date.now(); // Timestamp do momento em que a conexão abriu
  socket.canais = [];
  socket.modes = '';

  server.clients.push(socket);
  utils.setupTimeoutControl(socket);

  // Processa o recebimento de mensagens
  let msg = '';
  socket.on('data', function(data) {
    if (socket.pingTimer) {
      utils.setupTimeoutControl(socket);
      socket.lastInteraction = Date.now();
    }

    msg += data.toString();
    if (msg.endsWith('\n')) {
      msg = msg.trim();
      analisar(socket, msg);
      msg = '';
    }
  });

  // Processa a finalização da conexão
  socket.on('end', function() {
    if (socket.identifier)
      server.clients.splice(server.clients.indexOf(socket), 1);

    if (socket.nick)
      delete server.nicks[socket.nick];

    if (socket.user)
      delete server.users[socket.user];

    clearTimeout(socket.pingTimer);
    clearTimeout(socket.pongTimer);

    let canais = socket.canais.slice();
    for (let nomeCanal of canais) {
      let canal = server.canais[nomeCanal];

      if (!canal.modes.includes('q') && !socket.modes.includes('i')) {
        if (socket.endMessage)
          utils.broadcast(`${socket.nick} saiu do canal com a mensagem: ${socket.endMessage}\r\n`, socket.name, nomeCanal);
        else
          utils.broadcast(`${socket.nick} saiu do canal!\r\n`, socket.name, nomeCanal);
      }

      utils.leaveChannel(server, socket, canal);
    }
  });
}).listen(server.port, function() {
  console.log(`Server listening on port ${server.port}...`);
  server.started = Date.now(); // Guarda o timestamp do momento em que o server iniciou
});

// Objeto que guarda informações para cada um dos comandos
//  count: quantidade de vezes que o comando foi executado
//  function: função que implementa a lógica desse comando
server.comandos = {
  // Registros de Conexão
  'NICK': {
    count: 0,
    function: RDG.nick
  },
  'USER': {
    count: 0,
    function: RDG.user
  },
  'OPER': {
    count: 0,
    function: RDG.oper
  },
  'PASS': {
    count:0,
    function: RDG.pass
  },
  'QUIT': {
    count:0,
    function: RDG.quit
  },
  // Operações de Canal
  'JOIN': {
    count: 0,
    function: ODC.joinChannel
  },
  'PART': {
    count: 0,
    function: ODC.partChannel
  },
  'MODE': {
    count: 0,
    function: ODC.mode
  },
  'TOPIC': {
    count: 0,
    function: ODC.topic
  },
  'LIST': {
    count: 0,
    function: ODC.list
  },
  'NAMES' : {
    count: 0,
    function: ODC.names
  },
  'INVITE' : {
    count: 0,
    function: ODC.invite
  },
  'KICK' : {
    count: 0,
    function: ODC.kick
  },
  // Envio de Mensagens
  'PRIVMSG': {
    count: 0,
    function: EDM.privmsg
  },
  // Consultas ao Servidor
  'TIME': {
    count: 0,
    function: COS.time
  },
  'MOTD': {
    count: 0,
    function: COS.motd
  },
  'LUSERS': {
    count: 0,
    function: COS.lusers
  },
  'VERSION':{
    count: 0,
    function: COS.version
  },
  'STATS':{
    count: 0,
    function: COS.stats
  },
  'INFO':{
    count: 0,
    function: COS.info
  },
  // Consultas Baseadas no Usuário
  'WHO': {
    count: 0,
    function: CBU.who
  },
  'WHOIS': {
    count: 0,
    function: CBU.whois
  },
  // Variadas
  'KILL': {
    count: 0,
    function: VAD.kill
  },
  'PING': {
    count: 0,
    function: VAD.ping
  },
  'PONG': {
    count: 0,
    function: VAD.pong
  }
};

function analisar(socket, mensagem) {
  console.log(`[>>MSG<<] ${socket.name}: ${mensagem}`); // Temporario - Apenas para ver o comando de entrada
  var args = mensagem.split(" ");

  if (socket.pongTimer && args[0] !== 'PONG')
    return socket.write('ERRO: Server está aguardando o retorno PONG!\r\n');

  // Checa a existencia do comando no objeto 'comandos'
  if (server.comandos.hasOwnProperty(args[0])) {
    let allowedCommands = ['PASS', 'NICK', 'USER', 'PONG'];
    if (!socket.identifier && !allowedCommands.includes(args[0]))
      return socket.write("ERRO: Identifique-se antes de executar o comando! (Utilize NICK e USER)\r\n");

    let comando = server.comandos[args[0]];
    comando.count++;
    comando.function(server, socket, args);
  } else {
    socket.write("ERRO: Comando inexistente!\r\n");
  }
}
