# Sistemas Distribuídos Projeto 1: Servidor IRC

## Membros

* Alvaro Rodrigues - Documentação e Desenvolvimento
* Daniel Santos - Desenvolvimento
* Felipe Saraiva - Líder
* Franklin Silva - Desenvolvimento
* Victor Carvalho - Desenvolvimento

## Wiki

Toda a lista de tarefas, assim como o andamento do projeto e procedimentos úteis estão localizados na wiki do GitLab.  
<https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/home>
