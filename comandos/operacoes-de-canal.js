const utils = require('../utils');

// JOIN
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-join
module.exports.joinChannel = function(server, socket, args) {
  if (!args[1])
    return socket.write("ERRO: Argumentos insuficientes! (JOIN #canal1[,#canal2,...] [chave1,...])\r\n");

  if (args[1] === '0') {
    let canais = socket.canais.slice();

    for (let nomeCanal of canais) {
      let canal = server.canais[nomeCanal];

      if (!canal.modes.includes('q') && !socket.modes.includes('i'))
        utils.broadcast(`${socket.nick} saiu do canal!\r\n`, socket.name, canal.name);

      utils.leaveChannel(server, socket, canal);
    }

    return socket.write('OK: Você saiu de todos os canais!\r\n');
  }

  let listaCanais = args[1].split(',');
  let listaChaves = (args[2] || '').split(',');
  listaCanais.forEach(function(nomeCanal, index) {
    if (!nomeCanal.startsWith('#') || /[^\w-]/g.test(nomeCanal.slice(1)))
      return socket.write(`ERRO: O canal ${nomeCanal} é inválido!\r\n`);

    let canal = server.canais[nomeCanal];

    if (canal) {
      if (canal.clients.includes(socket))
        return socket.write(`ERRO: Você já faz parte do canal ${nomeCanal}!\r\n`);

      if (canal.maxClients && canal.clients.length >= canal.maxClients)
        return socket.write(`ERRO: O canal ${nomeCanal} atingiu seu limite máximo de participantes!\r\n`);

      if (canal.key && !(listaChaves[index] === canal.key))
        return socket.write(`ERRO: A chave do canal ${nomeCanal} está incorreta!\r\n`);

      if (canal.modes.includes('i')) {
        if (canal.invited.includes(socket))
          canal.invited.splice(canal.invited.indexOf(socket), 1);
        else
          return socket.write(`ERRO: É necessário um convite para entrar no canal ${nomeCanal}!\r\n`);
      }

      socket.canais.push(nomeCanal);
      canal.clients.push(socket);

      if (!canal.modes.includes('q') && !socket.modes.includes('i'))
        utils.broadcast(`${socket.nick} entrou no canal!\r\n`, '', nomeCanal);
    } else {
      // Criação de um novo canal
      canal = {
        name: nomeCanal,
        clients: [socket], // Array de usuarios no canal
        topic: '', // Tema do canal
        creator: socket, // Criador do canal (Sempre tem privilegio de operador)
        operators: [], // Array de operadores do canal
        voice: [], // Array de clientes permitidos a enviar mensagens no caso de um canal moderado
        modes: 't',
        maxClients: 0,
        invited: [], // Array associativo de nicks convidados
        key: (listaChaves[index] ? listaChaves[index] : '')
      };
      server.canais[nomeCanal] = canal;

      socket.canais.push(nomeCanal);
      socket.write(`O canal ${nomeCanal} foi criado!\r\n`);
    }
  });
}

// PART
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-part
// TODO: Adicionar suporte para o argumento * (Sair de todos os canais)
module.exports.partChannel = function(server, socket, args) {
  if (!args[1])
    return socket.write("ERRO: Argumentos insuficientes! (PART canal1[,canal2,...] [:motivo])\r\n");

  // Mensagem que o usuário enviou como motivo da saida
  let msg = args.join(' ').split(':')[1];
  msg = msg ? msg.trim() : msg;

  let listaCanais = args[1].split(',');
  if (args[1] === '*')
    listaCanais = socket.canais.slice();

  listaCanais.forEach(function(nomeCanal) {
    if (!nomeCanal.startsWith('#'))
      return socket.write(`ERRO: O canal ${nomeCanal} é inválido!\r\n`);

    let canal = server.canais[nomeCanal];

    if (!canal)
      return socket.write(`ERRO: O canal ${nomeCanal} não existe!\r\n`);

    if (!canal.clients.includes(socket))
      return socket.write(`Você não faz parte do canal ${nomeCanal}!\r\n`);

    // Envia inclusive para o autor da ação
    if (!canal.modes.includes('q') && !socket.modes.includes('i')) {
      if (msg)
        utils.broadcast(`${socket.nick} saiu do canal com a mensagem: ${msg}\r\n`, '', nomeCanal);
      else
        utils.broadcast(`${socket.nick} saiu do canal!\r\n`, '', nomeCanal);
    }

    utils.leaveChannel(server, socket, canal);
  });
}

/* MODE - User & Channel
 * Casos de Teste Usuario: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-mode-usuario
 * Casos de Teste Canal: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-mode-canal
 * Apenas um modificador é aceito por comando
 * Modos do Usuario:
 *  i - invisible
 *  o - operator (Do servidor. Não deve ser aceita a adição dessa flag atraves do comando MODE)
 *
 * Modos de Canal:
 *  O - criador do canal (MODE #channel O - Apenas para vizualizar)
 *  o - operador (MODE #channel +o username / MODE #channel -o username)
 *  v - voz (MODE #channel +v username / MODE #channel -v username)
 *  i - apenas por convite (MODE #channel +i / MODE #channel -i)
 *  m - moderado (MODE #channel +m / MODE #channel -m)
 *  n - apenas membros (MODE #channel +n / MODE #channel -n)
 *  q - quieto (MODE #channel +q / MODE #channel -q)
 *  t - apenas operadores alteram topico (MODE #channel +t / MODE #channel -t)
 *  l - limite de usuarios (MODE #channel +l 12 / MODE #channel -l)
 *  k - chave (MODE #channel +k oulu / MODE #channel -k)
 */
module.exports.mode = function(server, socket, args) {
  // Valida a quantidade de argumentos
  if (args.length == 1)
    return socket.write(socket.modes + '\r\n');
  else if (args.length < 3 )
    return socket.write('ERRO: Argumentos insuficientes! (MODE alvo modos [parametros])\r\n');

  let target = args[1];
  let modes = args[2];

  let operator = args[2].charAt(0);
  operator = operator === '+' || operator === '-' ? operator : '';
  let mode = modes.replace(operator, '');

  // Garante que apenas um modo foi fornecido
  if (mode.length > 1)
    return socket.write('ERRO: Não é permitido utilizar mais de um modificador no mesmo comando!\r\n');

  // Trata o caso de o alvo ser um nickname (User Mode)
  if (!target.startsWith('#')) {
    if (!operator)
      return socket.write('ERRO: Operador (+ ou -) não fornecido!\r\n');

    if (!(target === socket.nick))
      return socket.write(`ERRO: Só é permitido alterar seu prório modo!\r\n`);

    let client = server.nicks[target];

    switch (mode) {
      case 'i':
        if (operator === '+' && !client.modes.includes('i'))
          client.modes += mode;
        else if (operator === '-')
          client.modes = client.modes.replace(mode, '');
        break;

      case 'o':
        if (operator === '+')
          return socket.write(`ERRO: Para ganhar permissões de operador, utilize o comando OPER!\r\n`);
        else if (operator === '-')
          client.modes = client.modes.replace(mode, '');
        break;

      default:
        return socket.write('ERRO: Modificador inválido!\r\n');
    }

    socket.write('OK: Comando executado com sucesso!\r\n');
    return;
  } // Fim do User Mode

  // Valida existência do canal
  if (!server.canais[target])
    return socket.write(`ERRO: O canal ${target} não existe!\r\n`);

  let canal = server.canais[target];

  // Trata o caso de vizualizar o criador do canal
  if (modes === 'O')
    return socket.write(`[${target}] O criador do canal é o(a) ${canal.creator.nick}\r\n`);

  // Verifica a obrigatoriedade do operador
  if (!operator)
    return socket.write('ERRO: Operador (+ ou -) não fornecido!\r\n');

  // Verifica permissão para alterar os outros modos
  if (!utils.isChannelOperator(canal, socket))
    return socket.write('ERRO: Você não possui permissão para executar esse comando!\r\n');

  switch (mode) {
    case 'o':
      {
        let nick = args[3];
        if (!nick)
          return socket.write('ERRO: Parâmetros do modificador insuficientes!\r\n');

        if (!server.nicks.hasOwnProperty(nick))
          return socket.write('ERRO: Nickname não existe!\r\n');

        let client = server.nicks[nick];

        if (!canal.clients.includes(client))
          return socket.write('ERRO: Usuário não participa desse canal!\r\n');

        if (operator === '+' && !canal.operators.includes(client)) {
          canal.operators.push(client);
          client.write(`[${target}] Você agora possui permissões de operador!\r\n`);
        } else if (operator === '-' && canal.operators.includes(client)) {
          canal.operators.splice(canal.operators.indexOf(client), 1);
          client.write(`[${target}] Suas permissões de operador foram removidas!\r\n`);
        }
        break;
      } // Fim do modo 'o'

    case 'v':
      {
        if (!canal.modes.includes('m'))
          return socket.write('ERRO: Esse modificador só é permitido em canais moderados (Modificador "m")!\r\n');

        let nick = args[3];
        if (!nick)
          return socket.write('ERRO: Parâmetros do modificador insuficientes!\r\n');

        if (!server.nicks.hasOwnProperty(nick))
          return socket.write('ERRO: Nickname não existe!\r\n');

        let client = server.nicks[nick];

        if (utils.isChannelOperator(canal, client))
          return socket.write('ERRO: Operadores já possuem permissão para falar!\r\n');

        if (operator === '+' && !canal.voice.includes(client)) {
          canal.voice.push(client);
          client.write(`[${target}] Você agora possui permissão de fala!\r\n`);
        } else if (operator === '-' && canal.voice.includes(client)) {
          canal.voice.splice(canal.voice.indexOf(client), 1);
          client.write(`[${target}] Sua permissão de fala foi removida!\r\n`);
        }
        break;
      } // Fim do modo 'v'

    case 'l':
      {
        if (operator === '+') {
          if (!args[3])
            return socket.write('ERRO: Parâmetros do modificador insuficientes!\r\n');

          let max = Number(args[3]);
          if (Number.isNaN(max))
            return socket.write('ERRO: O máximo de usuarios deve ser um número!\r\n');

          if (max <= 0)
            return socket.write('ERRO: O máximo de usuarios deve ser no mínimo um!\r\n');

          canal.maxClients = max;
        } else {
          canal.maxClients = 0;
        }
        break;
      } // Fim do modo 'l'

    case 'k':
      {
        if (operator === '+') {
          if (!args[3])
            return socket.write('ERRO: Parâmetros do modificador insuficientes!\r\n');

          let pass = args[3];
          if (pass.includes(','))
            return socket.write('ERRO: Chave não pode incluir vírgulas!\r\n');

          canal.key = pass;

          if (!canal.modes.includes('q'))
            utils.broadcast(`Chave alterada para "${pass}"!\r\n`, '', target);
        } else {
          canal.key = '';

          if (!canal.modes.includes('q'))
            utils.broadcast(`Chave removida!\r\n`, '', target);
        }
        break;
      } // Fim do modo 'k'

    case 'i':
    case 'm':
    case 'n':
    case 'q':
    case 't':
      if (operator === '+' && !canal.modes.includes(mode)) {
        canal.modes += mode;
      } else if (operator === '-') {
        canal.modes = canal.modes.replace(mode, '');

        if (mode === 'm')
          canal.voice = [];

        if (mode === 'i')
          canal.invited = [];
      }
      break;

    default:
      return socket.write('ERRO: Modificador inválido!\r\n');
  }

  socket.write('OK: Comando executado com sucesso!\r\n');
}

// TOPIC
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-topic
module.exports.topic = function(server, socket, args) {
  let nomeCanal = args[1];
  let mudarTopico = args.join(' ').split(':').length > 1;
  let novoTopico = args.join(' ').split(':')[1];
  novoTopico = novoTopico ? novoTopico.trim() : novoTopico;

  if (!nomeCanal)
    return socket.write("ERRO: Argumentos insuficientes! (TOPIC canal[ :novo_tema])\r\n");

  if (!(nomeCanal.startsWith('#') && server.canais[nomeCanal]))
    return socket.write(`ERRO: O canal ${nomeCanal} não existe!\r\n`);

  let canal = server.canais[nomeCanal];

  // Apenas lê o tema do canal
  if (!mudarTopico) {
    if (canal.topic)
      return socket.write(`[${nomeCanal}] ${canal.topic}\r\n`);
    else
      return socket.write(`O canal ${nomeCanal} não possui tema!\r\n`);
  }

  if (canal.modes.includes('t') && !utils.isChannelOperator(canal, socket))
    return socket.write("ERRO: Você não possui permissão para executar esse comando!\r\n");

  canal.topic = novoTopico;

  if (!canal.modes.includes('q'))
    utils.broadcast(`Tema alterado para "${canal.topic}"\r\n`, socket.name, nomeCanal);
}

// LIST
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-list
module.exports.list = function(server, socket, args) {
  let list = Object.keys(server.canais);
  if (args[1] && args[1] !== '*')
    list = args[1].split(',');

  list.sort();
  socket.write('Lista de canais:\r\n');

  for (let nomeCanal of list) {
    if (!server.canais.hasOwnProperty(nomeCanal))
      continue;

    let canal = server.canais[nomeCanal];
    socket.write(canal.topic ? `${canal.name}: ${canal.topic}\r\n` : `${canal.name}\r\n`);
  }

  socket.write('Fim da lista de canais\r\n');
}

// NAMES
// CAsos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-names
module.exports.names = function(server, socket, args) {
  let listaCanais = Object.keys(server.canais);
  if (args[1])
    listaCanais = args[1].split(',');

  listaCanais.sort();

  for (let nomeCanal of listaCanais) {
    if (!server.canais.hasOwnProperty(nomeCanal))
      continue;

    let canal = server.canais[nomeCanal];
    let users = canal.clients.reduce(function(str, client) {
      // Exclui os clientes invisiveis
      if (client.modes.includes('i'))
        return str;

      // Adiciona marcadores de operador ou de voz
      if (utils.isChannelOperator(canal, client))
        str += '@';
      else if (canal.voice.includes(client))
        str += '+';

      return str + client.nick + ' ';
    }, "");

    socket.write(`${canal.name}: ${users}\r\n`);
  }

  // Lista os usuários que não estão em nenhum canal
  let nenhum = server.clients.reduce(function(str, client) {
    // Exclui os clientes invisiveis
    if (!client.modes.includes('i') && client.canais.length === 0)
      return str + client.nick + ' ';
    else
      return str;
  }, "");

  if (nenhum.length > 0)
    socket.write(`Nenhum canal: ${nenhum}\r\n`);

  socket.write('Fim da lista NAMES\r\n');
}

// KICK
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-kick
// Comando limitado a um canal por vez, mas pode ter varios nicks
module.exports.kick = function(server, socket, args) {
  if (args.length < 3)
    return socket.write("ERRO: Argumentos insuficientes! (KICK canal nick1[,nick2,...] [:motivo])\r\n");

  let nomeCanal = args[1];
  if (!server.canais.hasOwnProperty(nomeCanal))
    return socket.write(`ERRO: O canal ${nomeCanal} não existe!\r\n`);

  let canal = server.canais[nomeCanal];
  if (!utils.isChannelOperator(canal, socket))
    return socket.write("ERRO: Você não possui permissão para executar esse comando!\r\n");

  let listaNicks = args[2].split(',');
  let motivo = args.join(' ').split(':')[1];
  motivo = motivo ? motivo.trim() : motivo;

  for (let nick of listaNicks) {
    if (!server.nicks.hasOwnProperty(nick))
      continue;

    let client = server.nicks[nick];
    if (canal.clients.includes(client)) {
      // Apenas o criador pode expulsar operadores
      if (canal.creator !== socket && utils.isChannelOperator(canal, client)) {
        socket.write(`ERRO: Você não pode expulsar outro operador! (${nick})\r\n`);
        continue;
      }

      if (motivo)
        client.write(`[${nomeCanal}] O operador ${socket.nick} te removeu por: ${motivo}\r\n`);
      else
        client.write(`[${nomeCanal}] O operador ${socket.nick} te removeu do canal!\r\n`);

      if (!canal.modes.includes('q') && !socket.modes.includes('i'))
        utils.broadcast(`${client.nick} foi removido do canal!\r\n`, client.name, nomeCanal);

      utils.leaveChannel(server, client, canal);
    }
  }
}

// INVITE
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-invite
module.exports.invite = function (server, socket, args) {
  let nick = args[1];
  let nomeCanal = args[2];

  if (!nick || !nomeCanal)
    return socket.write("ERRO: Argumentos insuficientes! (INVITE nick canal)\r\n");

  if (!server.nicks.hasOwnProperty(nick))
    return socket.write("ERRO: Nickname não existe!\r\n");

  let client = server.nicks[nick];

  if (server.canais.hasOwnProperty(nomeCanal)) {
    let canal = server.canais[nomeCanal];

    if (!canal.clients.includes(socket))
      return socket.write('ERRO: Apenas membros do canal podem convidar outros usuários!\r\n');

    if (canal.modes.includes('i') && !utils.isChannelOperator(canal, socket))
      return socket.write('ERRO: Apenas operadores do canal podem convidar outros usuários!\r\n');

    if (canal.clients.includes(client))
      return socket.write('ERRO: Usuário já está no canal!\r\n');

    if (canal.modes.includes('i') && !canal.invited.includes(client))
      canal.invited.push(client);

    client.write(`${socket.nick} está te convidando para o canal ${nomeCanal}\r\n`);
  } else {
    client.write(`${socket.nick} está te convidando para um novo canal ${nomeCanal}\r\n`);
  }

  socket.write('OK: Convite enviado com sucesso!\r\n');
}
