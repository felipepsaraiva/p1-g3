const utils = require('../utils');

// KILL
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-kill
module.exports.kill = function(server, socket, args) {
  if (!socket.modes.includes('o'))
    return socket.write('ERRO: Você não possui permissão para executar esse comando!\r\n');

  if (args.length < 2)
    return socket.write("ERRO: Argumentos insuficientes! (KILL nickname [:motivo])\r\n");

  let nickname = args[1];
  if (!server.nicks.hasOwnProperty(nickname))
    return socket.write('ERRO: Nickname não existe!\r\n');

  let client = server.nicks[nickname];
  let motivo = args.join(' ').split(':')[1];

  if (motivo)
    client.end(`Sua conexão foi finalizada por um operador com o motivo: ${motivo}\r\n`);
  else
    client.end(`Sua conexão foi finalizada por um operador!\r\n`);
}

// PING
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-ping
module.exports.ping = function(server, socket, args) {
  socket.write(`PONG :${utils.getIP4(socket.localAddress)}\r\n`);
}

// PONG
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-pong
module.exports.pong = function(server, socket, args) {
  if (socket.pongTimer) {
    clearTimeout(socket.pongTimer);
    delete socket.pongTimer;

    utils.setupTimeoutControl(socket);
  }
}
