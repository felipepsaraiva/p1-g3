const utils = require('../utils');

// WHO
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-who
module.exports.who = function(server, socket, args) {
  let nomeCanal = args[1];
  if (nomeCanal) {
    if (!server.canais.hasOwnProperty(nomeCanal))
      return socket.write(`ERRO: O canal ${nomeCanal} não existe!\r\n`);

    let canal = server.canais[nomeCanal]
    for (let client of canal.clients) {
      if (client.modes.includes('i'))
      continue;

      let permissoes = "";
      if (client.modes.includes('o'))
        permissoes += '*';

      if (utils.isChannelOperator(canal, client))
        permissoes += '@';
      else if (canal.voice.includes(client))
        permissoes += '+';

      socket.write(`${nomeCanal} ${client.user} ${utils.getIP4(client.remoteAddress)} ${utils.getIP4(client.localAddress)} ${client.nick} ${permissoes} :0 ${client.fullName}\r\n`);
    }
  } else {
    for (let client of server.clients) {
      // Se o cliente for invisivel ou tiver canal em comum com o socket, passa para o proximo
      if (client.modes.includes('i') || client.canais.some(nomeCanal => socket.canais.includes(nomeCanal)))
        continue;

      socket.write(`* ${client.user} ${utils.getIP4(client.remoteAddress)} ${utils.getIP4(client.localAddress)} ${client.nick} ${socket.modes.includes('o') ? '*' : ''} :0 ${client.fullName}\r\n`);
    }
  }

  socket.write('Fim da lista WHO\r\n');
}

// WHOIS
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-whois
module.exports.whois = function(server, socket, args) {
  if (args.length < 2)
    return socket.write('ERRO: Argumentos insuficientes! (WHOIS nick)\r\n');

  let nickname = args[1];
  if (!server.nicks.hasOwnProperty(nickname))
    return socket.write('ERRO: Nickname não existe!\r\n');

  let client = server.nicks[nickname];
  if (client.modes.includes('i'))
    return socket.write('ERRO: Você não pode ver esse usuário!\r\n');

  socket.write(`${client.nick} é o usuário ${client.identifier.split('!')[1]} * :${client.fullName}\r\n`);

  let canais = client.canais.reduce(function(str, nomeCanal) {
    let canal = server.canais[nomeCanal];

    // Adiciona marcadores de operador ou de voz
    if (utils.isChannelOperator(canal, client))
      str += '@';
    else if (canal.voice.includes(client))
      str += '+';

    return str + canal.name + ' ';
  }, "");

  if (canais.length > 0)
    socket.write(`${client.nick} está nos canais: ${canais}\r\n`);

  socket.write(`${client.nick} está usando o servidor ${utils.getIP4(client.localAddress)}\r\n`);
  socket.write(`${client.nick} está inativo por ${Math.floor((Date.now() - client.lastInteraction)/1000)} segundos, entrou ${new Date(client.joined)}\r\n`);

  if (client.modes.includes('o'))
    socket.write(`${client.nick} é um operador do servidor\r\n`);

  socket.write(`Fim da lista WHOIS\r\n`);
}
