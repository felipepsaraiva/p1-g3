const utils = require('../utils');

// PRIVMSG
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-privmsg
module.exports.privmsg = function(server, socket, args) {
  let receiver = args[1];
  let msg = args.join(' ').split(':')[1];
  msg = msg ? msg.trim() : msg;

  if (!receiver || !msg)
    return socket.write("ERRO: Argumentos insuficientes! (PRIVMSG alvo_da_msg :mensagem)\r\n");

  if (server.nicks.hasOwnProperty(receiver)) {
    // Mensagem direta para usuario
    server.nicks[receiver].write(`${socket.nick}: ${msg}\r\n`);
  } else if (server.canais.hasOwnProperty(receiver)) {
    // Mensagem para canal
    let canal = server.canais[receiver];

    // Valida mensagem para canal com flag 'n'
    if (canal.modes.includes('n') && !canal.clients.includes(socket))
      return socket.write("ERRO: Você precisa participar desse canal para enviar mensagens!\r\n");

    // Valida mensagem para canal com flag 'm'
    if (canal.modes.includes('m')) {
      let allowed = (canal.voice.includes(socket) || canal.operators.includes(socket) || canal.creator === socket);
      if (!allowed)
        return socket.write("ERRO: O canal é moderado e você não tem permissão de fala!\r\n");
    }

    utils.broadcast(`${socket.nick}: ${msg}\r\n`, socket.name, receiver);
  } else {
    socket.write("ERRO: Alvo da mensagem não existe! (PRIVMSG alvo_da_msg :mensagem)\r\n");
  }
}
