const utils = require('../utils');

// TIME
// Casos de Teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-time
module.exports.time = function(server, socket, args) {
  let ip4 = utils.getIP4(socket.localAddress.toString());

  if ((args[1] == ip4 || args[1] == undefined)) {
    let date = new Date();

    socket.write(ip4 + ", " + date.toString() + "\r\n");
  } else {
    socket.write("ERRO: Server não existe!\r\n");
  }
}

// MOTD
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-motd
module.exports.motd = function(server, socket, args) {
  let ip4 = utils.getIP4(socket.localAddress.toString());

  if ((args[1] == ip4 || args[1] == undefined)) {
    utils.sendMOTD(socket);
  } else {
    socket.write("ERRO: Server não existe!\r\n");
  }
}

// LUSERS
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-lusers
module.exports.lusers = function(server, socket, args) {
  let unknownCount = server.clients.reduce((sum, client) => sum + (client.identifier ? 0 : 1), 0);
  let invisibleCount = server.clients.reduce((sum, client) => sum + (client.modes.includes('i') ? 1 : 0), 0);
  let operatorCount = server.clients.reduce((sum, client) => sum + (client.modes.includes('o') ? 1 : 0), 0);

  socket.write(`Existe(m) ${server.clients.length - unknownCount} usuário(s), sendo ${invisibleCount} invisível(is), em 1 servidor\r\n`);
  socket.write(`Operadores online: ${operatorCount}\r\n`);
  socket.write(`Conexões desconhecidas: ${unknownCount}\r\n`);
  socket.write(`Canais criados: ${Object.keys(server.canais).length}\r\n`);
  socket.write(`Eu tenho ${server.clients.length} cliente(s) em 1 servidor\r\n`);
}

// VERSION
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-version
module.exports.version = function(server, socket, args) {
  socket.write("Versão do servidor: " + server.version + "\r\n");
}

// STATS
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-stats
module.exports.stats = function(server, socket, args) {
  if (!socket.modes.includes('o'))
    return socket.write('ERRO: Você não possui permissão para executar esse comando!\r\n');

  let query = args[1];
  if (!query)
    return socket.write('Fim da lista STATS\r\n');

  switch (query) {
    case 'l':
      for (let client of server.clients)
        socket.write(`${client.name} - Bytes recebidos: ${client.bytesRead} / Bytes enviados: ${client.bytesWritten} - Tempo aberto: ${Math.floor((Date.now() - client.opened)/1000)} segundos\r\n`);
      break;

    case 'm':
      for (let comando of Object.keys(server.comandos)) {
        let count = server.comandos[comando].count;
        if (count > 0)
          socket.write(`Comando ${comando} foi executado ${count} vez(es)\r\n`);
      }
      break;

    case 'o':
      let json = require('../oper-users.json');
      let operadores = json.users.reduce((str, oper) => str + oper.username + ' ', '');
      socket.write(`Nome de usuario operadores: ${operadores}\r\n`);
      break;

    case 'u':
      let uptime = Math.floor((Date.now() - server.started) / 1000);
      socket.write(`Servidor ligado por ${Math.floor(uptime / 86400)} dias, ${Math.floor((uptime % 86400) / 3600)} horas, ${Math.floor((uptime % 3600) / 60)} minutos e ${uptime % 60} segundos\r\n`);
      break;

    default:
      return socket.write(`ERRO: O argumento ${query} é inválido!\r\n`);
  }

  socket.write('Fim da lista STATS\r\n');
}

// INFO
// Casos de teste: https://gitlab.com/sd1-ec-2017-2/p1-g3/wikis/testes/comando-info
module.exports.info = function(server, socket, args) {
  socket.write(`Versão do servidor: ${server.version}\r\n`);
  socket.write(`Data do Release:  ${server.release}\r\n`);
  socket.write(`Online desde: ${new Date(server.started).toString()} \r\n`);
  socket.write(`Fim da lista INFO\r\n`);
}
