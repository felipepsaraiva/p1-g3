const server = require('./server.js');

// Manda uma mensagem para todos os clientes do chat ou de um canal
module.exports.broadcast = function(message, senderName, channelName) {
  let sendTo = channelName && server.canais[channelName] ? server.canais[channelName].clients : server.clients;
  message = (channelName ? `[${channelName}] ` : '') + message;

  sendTo.forEach(function(client) {
    // Evita eco da mensagem
    if (client.name === senderName)
      return;

    client.write(message);
  });

  process.stdout.write(message);
};

// Remove o cliente do canal fazendo o tratamento necessário
module.exports.leaveChannel = function(server, socket, canal) {
  socket.canais.splice(socket.canais.indexOf(canal.name), 1); // Remove o canal da lista de canais que o socket participa
  canal.clients.splice(canal.clients.indexOf(socket), 1); // Remove o usuario da lista de clientes do canal

  if (canal.clients.length == 0) {
    delete server.canais[canal.name];
    return;
  }

  // Remove o cliente da lista de operadores
  if (canal.operators.includes(socket))
    canal.operators.splice(canal.operators.indexOf(socket), 1);

  // Remove o cliente da lista de usuarios com permissao de fala
  if (canal.voice.includes(socket))
    canal.voice.splice(canal.voice.indexOf(socket), 1);

  if (canal.creator == socket) {
    delete canal.creator;
    
    if (canal.operators.length > 0) {
      // Se o criador saiu e existe operadores, coloca o primeiro como criador do canal
      canal.creator = canal.operators[0];
      canal.operators.splice(0, 1);
      canal.creator.write(`Você recebeu privilégio de criador do canal ${canal.name}!\r\n`);
    }
  }
};

// Checa se o socket tem permissão de operador no canal
module.exports.isChannelOperator = function(canal, socket) {
  return (socket === canal.creator || canal.operators.includes(socket));
};

// Envia o MOTD para o socket fornecido
module.exports.sendMOTD = function(socket) {
  const fs = require('fs');
  fs.readFile('irc.motd', 'utf8', function(err, data) {
    if (err || Object.keys(data).length <= 1) {
      socket.write("ERRO: Servidor não possui MOTD\r\n");
      return;
    } else {
      socket.write('Mensagem do dia:\r\n');
      socket.write(data + "\r\n");
      socket.write('Fim da mensagem do dia\r\n');
    }
  });
};

// Extrai o IPv4 do IPv6
function getIP4(data) {
  let regex = /(\d.{1,15})/;
  let str = data;
  let host = str.match(regex);
  return host[1];
};
module.exports.getIP4 = getIP4;

module.exports.setupTimeoutControl = function(socket) {
  clearTimeout(socket.pingTimer);

  // Timer utilizado para acionar o PING
  // Após server.pingTimeout segundos de inatividade envia o PING
  socket.pingTimer = setTimeout(function() {
    socket.write(`PING :${getIP4(socket.localAddress)}\r\n`);
    delete socket.pingTimer;

    // O cliente tem server.pongTimeout segundos para responser ou a conexão fecha
    socket.pongTimer = setTimeout(() => socket.end(), server.pongTimeout * 1000);
  }, server.pingTimeout * 1000);
};
